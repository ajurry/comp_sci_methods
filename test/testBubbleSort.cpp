#include <vector>
#include <array>
#include <list>
#include <random>
#include <algorithm>

#include <gtest/gtest.h>

#include "bubbleSort/bubbleSort.hpp"

TEST(bubbleSortTest, VectorSizeZero){
    std::vector<int> vec;
    BubbleSort::bubbleSort(vec.begin(),vec.end());
}

TEST(bubbleSortTest, VectorSizeOne){
    std::vector<int> vec{1};
    BubbleSort::bubbleSort(vec.begin(),vec.end());
}

TEST(bubbleSortTest, VectorEven){
    std::vector<int> vec1{2,1};
    std::vector<int> vec2{1,2};
    BubbleSort::bubbleSort(vec1.begin(),vec1.end());
    EXPECT_EQ(vec1[0],vec2[0]);
    EXPECT_EQ(vec1[1],vec2[1]);
}

TEST(bubbleSortTest, VectorOdd){
    std::vector<int> vec1{3,1,2};
    std::vector<int> vec2{1,2,3};
    BubbleSort::bubbleSort(vec1.begin(),vec1.end());
    EXPECT_EQ(vec1[0],vec2[0]);
    EXPECT_EQ(vec1[1],vec2[1]);
    EXPECT_EQ(vec1[2],vec2[2]);
}

TEST(bubbleSortTest, SortsVectorInt) {
    std::vector<int> vec1{15,2,3,4,5,6,7,8,9,10,11,12,13,14,1};
    std::vector<int> vec2{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    std::random_device rd;
    std::mt19937 g(rd());
    int testCount = 100;
    while(testCount != 0){
        std::shuffle(vec1.begin(),vec1.end(),g);
        BubbleSort::bubbleSort(vec1.begin(),vec1.end());
        EXPECT_TRUE(std::equal(vec1.begin(),vec1.end(),vec2.begin()));
        --testCount;
    }
}

TEST(bubbleSortTest, SortsVectorFloat) {
    std::vector<float> vec1{15,2,3,4,5,6,7,8,9,10,11,12,13,14,1};
    std::vector<float> vec2{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    std::random_device rd;
    std::mt19937 g(rd());
    int testCount = 100;
    while(testCount != 0){
        std::shuffle(vec1.begin(),vec1.end(),g);
        BubbleSort::bubbleSort(vec1.begin(),vec1.end());
        EXPECT_TRUE(std::equal(vec1.begin(),vec1.end(),vec2.begin()));
        --testCount;
    }
}

TEST(bubbleSortTest, SortsArray) {
    std::array<int,15> arr1{15,2,3,4,5,6,7,8,9,10,11,12,13,14,1};
    std::array<int,15> arr2{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    std::random_device rd;
    std::mt19937 g(rd());
    int testCount = 100;
    while(testCount != 0){
        std::shuffle(arr1.begin(),arr1.end(),g);
        BubbleSort::bubbleSort(arr1.begin(),arr1.end());
        EXPECT_TRUE(std::equal(arr1.begin(),arr1.end(),arr2.begin()));
        --testCount;
    }
}

TEST(bubbleSortTest, SortsList) {
    std::list<int> bilist1 {15,2,3,4,5,6,7,8,9,10,11,12,13,14,1};
    std::list<int> bilist2 {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    std::random_device rd;
    std::mt19937 g(rd());
    int testCount = 1;
    while(testCount != 0){
        BubbleSort::bubbleSort(bilist1.begin(),bilist1.end());
        EXPECT_TRUE(std::equal(bilist1.begin(),bilist1.end(),bilist2.begin()));
        --testCount;
    }

}
