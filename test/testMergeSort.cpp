#include <vector>
#include <array>
#include <list>
#include <random>
#include <algorithm>

#include <gtest/gtest.h>

#include "mergeSort/mergeSort.hpp"

TEST(mergeSortTest, VectorSizeZero){
    std::vector<int> vec;
    MergeSort::mergeSort(vec.begin(),vec.end());
}

TEST(mergeSortTest, VectorSizeOne){
    std::vector<int> vec{1};
    MergeSort::mergeSort(vec.begin(),vec.end());
}

TEST(mergeSortTest, VectorEven){
    std::vector<int> vec1{2,1};
    std::vector<int> vec2{1,2};
    MergeSort::mergeSort(vec1.begin(),vec1.end());
    EXPECT_EQ(vec1[0],vec2[0]);
    EXPECT_EQ(vec1[1],vec2[1]);
}

TEST(mergeSortTest, VectorOdd){
    std::vector<int> vec1{3,1,2};
    std::vector<int> vec2{1,2,3};
    MergeSort::mergeSort(vec1.begin(),vec1.end());
    EXPECT_EQ(vec1[0],vec2[0]);
    EXPECT_EQ(vec1[1],vec2[1]);
    EXPECT_EQ(vec1[2],vec2[2]);
}

TEST(mergeSortTest, SortsVectorInt) {
    std::vector<int> vec1{15,2,3,4,5,6,7,8,9,10,11,12,13,14,1};
    std::vector<int> vec2{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    std::random_device rd;
    std::mt19937 g(rd());
    int testCount = 100;
    while(testCount != 0){
        std::shuffle(vec1.begin(),vec1.end(),g);
        MergeSort::mergeSort(vec1.begin(),vec1.end());
        EXPECT_TRUE(std::equal(vec1.begin(),vec1.end(),vec2.begin()));
        --testCount;
    }
}

TEST(mergeSortTest, SortsVectorFloat) {
    std::vector<float> vec1{15,2,3,4,5,6,7,8,9,10,11,12,13,14,1};
    std::vector<float> vec2{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    std::random_device rd;
    std::mt19937 g(rd());
    int testCount = 100;
    while(testCount != 0){
        std::shuffle(vec1.begin(),vec1.end(),g);
        MergeSort::mergeSort(vec1.begin(),vec1.end());
        EXPECT_TRUE(std::equal(vec1.begin(),vec1.end(),vec2.begin()));
        --testCount;
    }
}

TEST(mergeSortTest, SortsArray) {
    std::array<int,15> arr1{15,2,3,4,5,6,7,8,9,10,11,12,13,14,1};
    std::array<int,15> arr2{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    std::random_device rd;
    std::mt19937 g(rd());
    int testCount = 100;
    while(testCount != 0){
        std::shuffle(arr1.begin(),arr1.end(),g);
        MergeSort::mergeSort(arr1.begin(),arr1.end());
        EXPECT_TRUE(std::equal(arr1.begin(),arr1.end(),arr2.begin()));
        --testCount;
    }
}

TEST(mergeSortTest, SortsList) {
    std::list<int> bilist1 {15,2,3,4,5,6,7,8,9,10,11,12,13,14,1};
    std::list<int> bilist2 {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    std::random_device rd;
    std::mt19937 g(rd());
    int testCount = 1;
    while(testCount != 0){
        MergeSort::mergeSort(bilist1.begin(),bilist1.end());
        EXPECT_TRUE(std::equal(bilist1.begin(),bilist1.end(),bilist2.begin()));
        --testCount;
    }

}
