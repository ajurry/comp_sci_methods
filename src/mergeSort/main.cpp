#include <vector>
#include <iterator>
#include <algorithm>
#include <iostream>
#include <random>
#include <array>

#include "mergeSort/mergeSort.hpp"

int main(){

    std::array<int,5> arr;
    std::vector<int> vec;

    int count = 5;

    while(count != 0){
        arr[count-1]=count;
        vec.push_back(count);
        --count;
    }

    std::random_device rd;
    std::mt19937 g(rd());

    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout," "));
    std::cout<<std::endl;

    std::shuffle(arr.begin(), arr.end(), g);
    std::copy(arr.begin(), arr.end(), std::ostream_iterator<int>(std::cout," "));
    std::cout<<std::endl;

    MergeSort::mergeSort(vec.begin(), vec.end());
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout," "));
    std::cout<<std::endl;

    MergeSort::mergeSort(arr.begin(), arr.end());
    std::copy(arr.begin(), arr.end(), std::ostream_iterator<int>(std::cout," "));
    std::cout<<std::endl;

    return 0;
}
