#ifndef bubbleSort_HPP
#define bubbleSort_HPP

#include <iterator>
#include <iostream>
#include <exception>
#include <algorithm>

namespace{

template <typename Iter>
void bubbleSortImp(Iter lptr, Iter rptr)
{
    bool changeFlag = false;
    auto first = lptr;
    auto second = ++lptr;
    --lptr;

    while(second!=rptr){
        if(*first>*second){
            std::swap(*first,*second);
            changeFlag = true;
        }
        ++first;
        ++second;
    }

    if(changeFlag){
        bubbleSortImp(lptr,rptr);
    } else {
        return;
    }
}

};

namespace BubbleSort{

template <typename RAIter>
void bubbleSort(RAIter lptr, RAIter rptr, std::random_access_iterator_tag){
    bubbleSortImp(lptr, rptr);
}

template <typename BIter>
void bubbleSort(BIter lptr, BIter rptr, std::bidirectional_iterator_tag){
    bubbleSortImp(lptr, rptr);
}

template <typename Iter>
void bubbleSort(Iter lptr, Iter rptr){
    if(lptr==rptr){
        std::cerr<<"No Items in Container"<<std::endl;
    }
    else{
        bubbleSort(lptr, rptr, typename std::iterator_traits<Iter>::iterator_category());
    }
}

};

#endif
