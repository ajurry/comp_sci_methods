#ifndef QUICKSORT_HPP
#define QUICKSORT_HPP

#include <iterator>
#include <iostream>
#include <exception>
#include <algorithm>

namespace{

template <typename Iter>
Iter partition(Iter pivot, Iter lptr, Iter rptr);

template <typename Iter>
void quickSortImp(Iter lptr, Iter rptr)
{
    if (lptr<rptr){
        Iter temp = partition(rptr,lptr,rptr);
        if(lptr<temp-1) quickSortImp(lptr,temp-1);
        if(temp+1<rptr) quickSortImp(temp+1,rptr);
    }
    return;
}

template <typename Iter>
Iter partition(Iter pivot, Iter lptr, Iter rptr)
{
    while(lptr<rptr)
    {
        while((*lptr<=*pivot) && lptr<=rptr){
            ++lptr;
        }

        while((*pivot<*rptr) && lptr<=rptr){
            --rptr;
        }

        if(lptr<rptr){
            std::swap(*lptr,*rptr);
        }
    }
    return rptr;
}

};

namespace QuickSort{

template <typename RAIter>
void quickSort(RAIter lptr, RAIter rptr, std::random_access_iterator_tag){
    quickSortImp(lptr, rptr);
}

template <typename Iter>
void quickSort(Iter lptr, Iter rptr){
    if(lptr==rptr){
        std::cerr<<"No Items in Container"<<std::endl;
    }
    else{
        --rptr;
        quickSort(lptr, rptr, typename std::iterator_traits<Iter>::iterator_category());
    }
}

};

#endif
