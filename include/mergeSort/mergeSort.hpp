#ifndef MERGE_SORT_HPP
#define MERGE_SORT_HPP

#include <iterator>
#include <algorithm>
#include <exception>
#include <iostream>

namespace{

//Note - Will Impletement Algorithm, Rather Than Using STL
template <typename Iter>
void inplaceMerge(Iter lptr, Iter middle, Iter rptr)
{
    std::inplace_merge(lptr,middle,rptr);
}

template <typename Iter>
void mergeSortImp(Iter lptr, Iter rptr)
{
    auto size = std::distance(lptr,rptr);

    if(size>1){
        auto middle = std::next(lptr,size/2);

        mergeSortImp(lptr,middle);
        mergeSortImp(middle,rptr);

        std::inplace_merge(lptr,middle,rptr);
    }
    return;
}

};

namespace MergeSort{

template <typename RAIter>
void mergeSort(RAIter lptr, RAIter rptr, std::random_access_iterator_tag){
    mergeSortImp(lptr, rptr);
}

template <typename BIter>
void mergeSort(BIter lptr, BIter rptr, std::bidirectional_iterator_tag){
    mergeSortImp(lptr, rptr);
}

template <typename Iter>
void mergeSort(Iter lptr, Iter rptr){
    if(lptr==rptr){
        std::cerr<<"No Items in Container"<<std::endl;
    }
    else{
        mergeSort(lptr, rptr, typename std::iterator_traits<Iter>::iterator_category());
    }
}

};

#endif
